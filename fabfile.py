from os.path import abspath, dirname, join
from contextlib import contextmanager as _contextmanager

from fabric.api import cd, lcd, run, sudo, env, prefix, local, settings, abort
from fabric.context_managers import shell_env
from fabric.contrib.console import confirm
from fabric.contrib.files import upload_template


GIT_URL = 'git@bitbucket.org:gearheart/{{ project_name }}.git'

env.local_path = dirname(abspath(__file__))

env.hosts = ['178.79.146.47']
env.user = 'web'
env.forward_agent = True

env.parent_path = '/web/'
env.project_path = '/web/{{ project_name }}/'
env.manage_path = '/web/{{ project_name }}/{{ project_name }}/'
env.activate = 'source /web/{{ project_name }}/venv/bin/activate'
env.requirements = '../requirements/production.txt'
env.settings_path = '{{ project_name }}.settings.staging'


@_contextmanager
def virtualenv():
    with cd(env.manage_path):
        with prefix(env.activate):
            yield


def _requirements_and_static():
    run('pip install -r {0}'.format(env.requirements))
    run('python manage.py bower_install --settings={0}'.format(env.settings_path))
    run('python manage.py collectstatic --noinput --settings={0}'.format(env.settings_path))
    run('python manage.py assets build --parse-templates --settings={0}'.format(env.settings_path))


def production():
    env.hosts = ['production_address']
    env.settings_path = '{{ project_name }}.settings.production'


def test():
    with settings(warn_only=True):
        with lcd(join(env.local_path, '{{ project_name }}')):
            result = local('./manage.py test', capture=True)
    if result.failed and not confirm("Tests failed. Continue anyway?"):
        abort("Aborting at user request.")


def update():
    test()
    with cd(env.project_path):
        run('git pull')
    with virtualenv():
        _requirements_and_static()
        run('python manage.py migrate --settings={0}'.format(env.settings_path))
    sudo('supervisorctl restart {{ project_name }}')


def deploy():
    test()
    with lcd(env.local_path):
        upload_template('config/nginx/{{ project_name }}.conf',
                        '/etc/nginx/sites-enabled/{{ project_name }}.conf',
                        use_sudo=True)
        upload_template('config/supervisor/{{ project_name }}.conf',
                        '/etc/supervisor/conf.d/{{ project_name }}.conf',
                        context={'settings_path': env.settings_path},
                        use_sudo=True)
    run('psql -c "CREATE DATABASE {{ project_name }} OWNER web" -h db postgres web')
    with cd(env.parent_path):
        run('git clone {0}'.format(GIT_URL))
    with cd(env.project_path):
        run('virtualenv venv')
    with virtualenv():
        _requirements_and_static()
        run('python manage.py syncdb --all --noinput --settings={0}'.format(env.settings_path))
        run('''echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')" | python manage.py shell --settings={0}'''.format(env.settings_path))
    sudo('supervisorctl reload')
    sudo('/etc/init.d/nginx restart')

# Local development dependencies go here
-r base.txt
coverage==3.6
django-coverage==1.2.4
django-discover-runner==0.4
django-debug-toolbar==0.9.4
Sphinx==1.2b1
Fabric==1.8.0